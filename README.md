# Semantic release management

## Requirements

* create an application auth token to tag on gitlab.com
* create an application auth token to upload on npmjs.org

## Testing release configuration locally

Set up NPM project in docker environment:
```
$ docker run --rm -v $PWD:/code -it node /bin/bash
# cd /code
```

Create new app:
```
# echo 'console.log("Hello world");' > index.js
# npm init -y

# npm -s start
Hello world

# npm -s test
All good
```

Install program and plugins:
```
# npm install -g semantic-release @semantic-release/gitlab
```

Set gitlab and npm registry configurations:
```
# export GL_TOKEN=<token>
# export GL_URL=https://gitlab.com
# export NPM_TOKEN=<token>
```

Run in dry-run mode:
```
# semantic-release -p @semantic-release/commit-analyzer,@semantic-release/release-notes-generator,@semantic-release/gitlab,@semantic-release/npm -b master -d
 ⚠  Run automated release from branch master in dry-run mode
 ✔  Allowed to push to the Git repository
 ℹ  Verify GitLab authentication (https://gitlab.com/api/v4)
 ℹ  Verify authentication for registry https://registry.npmjs.org/
 ℹ  Found 2 commits since last release
 ℹ  Analyzing commit: chore(ci): added gitlab CI config file
 ℹ  The commit should not trigger a release
 ℹ  Analyzing commit: feat(core): initialized app
 ℹ  The release type for the commit is minor
 ℹ  Analysis of 2 commits complete: minor release
 ℹ  There is no previous release, the next release version is 1.0.0
 ⚠  Skip v1.0.0 tag creation in dry-run mode
 ⚠  Skip step "publish" of plugin "@semantic-release/gitlab" in dry-run mode
 ✔  Published release 1.0.0
 ℹ  Release note for version 1.0.0:
# 1.0.0 (2018-12-23)

### Features

    * core: initialized app (09a00be (https://gitlab.com/plup/siblab-nodejs/commit/09a00be))
```

* [Configuration reference](https://semantic-release.gitbook.io/semantic-release/usage/configuration)

## Setup Gitlab CI configuration

Sample of `.gitlab-ci.yml` file:
```
---
image: node

stages:
  - test
  - release

before_script:
  - npm -s install

cache:
  paths:
    - node_modules/

test:
  stage: test
  script:
    - npm test

publish:
  stage: release
  before_script:
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release -p @semantic-release/commit-analyzer,@semantic-release/release-notes-generator,@semantic-release/gitlab,@semantic-release/npm -b master
  only:
    - master
```

## Test release management in pipeline

Make some commits:
```
$ git commit --allow-empty -m "fix(cd): triggers patch"
$ push

$ git commit --allow-empty -m "feat(release): triggers minor"
$ push

$ git commit --allow-empty -m "chore(doc): triggers nothing"
$ push

$ git commit --allow-empty -m 'chore(doc): triggers major
BREAKING CHANGE: Heads up !'
$ push
```

Check results:
```
$ git tag --list
v0.1.0
v0.1.1
v0.2.0
v1.0.0
```
